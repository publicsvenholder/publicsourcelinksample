﻿using System;
using System.Diagnostics;

namespace PublicSourceLinkLib
{
    public class PublicLib
    {

        public int Add(int a, int b)
        {
            // Some other
            var result = a + b;
            return result;
        }

        public void ThrowException()
        {
            throw new NotImplementedException();
        }

        public void HoldOnDebugger()
        {
            if (Debugger.IsAttached)
            {
                Debugger.Break();
            }

            Console.WriteLine("Debugging?");
        }
    }
}
